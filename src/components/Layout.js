import { Outlet } from "react-router-dom";
import Navbar  from "./Navbar";
import React from 'react'
import { useEffect, /* useState */ } from 'react';
import { useSelector } from "react-redux";

const Layout = () => {
  const cart = useSelector(state => state.cart.cart)
  const favorites = useSelector(state => state.favorites.favorites)

  

  /* FAVOURITES */

  /* const toggleFavourites = (item) => {
    if (favorites.some((el) => el.id === item.id)) {
      const updatedFavorites = favorites.filter((el) => el.id !== item.id);
      setFavorites(updatedFavorites);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    } else {
      const updatedFavorites = [...favorites, item];
      setFavorites(updatedFavorites);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    }
  }; */

  /* useEffect(() => {
    const storedFavorites = JSON.parse(localStorage.getItem('favorites'));
    if (storedFavorites) {
      setFavorites(storedFavorites);
    }
  }, []); */

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites])

  /* CART */

   useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);


  const cartSize = (cart) => {
    const mapingCart = cart.map((item) => {
      return item.count
    })

    if (mapingCart.length > 0) {
      const totalAmount = mapingCart.reduce(function (previousValue, currentValue) {
        return previousValue + currentValue
      })
      return totalAmount
    } else {
      return 0
    }
  } 
  
  return (
    <>
    <header>
        <Navbar cartSize={cartSize(cart)} favouritesSize={favorites.length}/>
    </header>
    {console.log(favorites)}

    <Outlet context={{
      // toggleFavourites,
      // favorites,
    }}/>
    </>
  )
}

export  {Layout}