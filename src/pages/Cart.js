// import { useOutletContext } from "react-router";
import CardsDelete from "../components/CardsDelete";
import '../style/cart.scss'
import { useSelector } from "react-redux";

const Cart = () => {
  // const { /* favorites, */ /* toggleFavourites */} = useOutletContext()
  const cart = useSelector(state => state.cart.cart)
  const favorites = useSelector(state => state.favorites.favorites)

  return (
    <article className="cart_container">
      {cart.map((item) => (
        <CardsDelete item={item} key={item.id} 
        boolean={favorites.some((el) => {return el.id === item.id})}/>
      ))}
    </article>
  );
};

export {Cart};
